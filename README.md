ШАГ 1. МОДУЛЬ ЯДРА
========================

Это модуль ядра, а следовательно, важно, чтобы ваше ядро его поддерживало. zRam включен в стандартную поставку начиная с ядра версии 3.14. Вы можете посмотреть вашу версию ядра командой:

```bash
uname -r
```

Проверка информации о модуле ядра:

```bash
modinfo zram
```

Если у вас более старое ядро, вам придется забыть про эту технологию или пересобрать его с этой настройкой. Модуль включается здесь:

Device Drivers --->

[*] Block devices --->

Compressed RAM block device support

ШАГ 2. НАСТРОЙКА GRUB
-------------------------

Когда модуль загружен надо установить параметр ядра zram.num_devices который указывает максимальное количество сжатых устройств. Например для двух блочных устройств используйте добавьте в переменную GRUB_CMDLINE_LINUX_DEFAULT следующий параметр:

```bash
sudo nano /etc/default/grub
```

```bash
GRUB_CMDLINE_LINUX_DEFAULT="...блаблабла... zram.num_devices=2"
```

Затем обновите конфигурацию Grub и перезагрузите компьютер:

```bash
sudo update-grub
sudo reboot
```

ШАГ 3. СКРИПТ ЗАПУСКА И ОСТАНОВКИ
-------------------------

Чтобы автоматизировать запуск zram желательно написать скрипт, который будет выполнять все необходимые действия. Давайте создадим скрипт, который запускает четыре zRam устройства и подключает их в качестве swap:

```bash
sudo nano /usr/local/bin/zram-start.sh
```

```bash
#!/bin/bash

modprobe --all lz4 lzo deflate

modprobe zram num_devices=2

echo '1024M' > /sys/block/zram0/disksize

echo '1024M' > /sys/block/zram1/disksize

mkswap /dev/zram0

swapon /dev/zram0 -p 10

mkswap /dev/zram1

swapon /dev/zram1 -p 10
```

Рассмотрим подробнее что означают строчки скрипта. Первая строчка с modprobe активирует модуля ядра на два блочных устройства. Следующие две строчки устанавливают размер для этих устройств в 1 Гб. Размер устанавливается в мегабайтах. Затем, для каждого блочного устройства создаем файловую систему подкачки командой mkswap и активируем его с помощью команды swapon. Теперь создадим скрипт, который выгружает всё это и отключает zram:

```bash
sudo nano /usr/local/bin/zram-stop.sh
```

```bash
#!/bin/bash

swapoff /dev/zram0

swapoff /dev/zram1

echo 1 > /sys/block/zram0/reset

echo 1 > /sys/block/zram1/reset

sleep .5

modprobe -r zram
```

Обоим скриптам надо дать права на выполнение:

```bash
sudo chmod ugo+x /usr/local/bin/zram-start.sh

sudo chmod ugo+x /usr/local/bin/zram-stop.sh
```

ШАГ 4. ТЕСТИРОВАНИЕ ZRAM

Теперь надо всё протестировать. Запустите zRam выполнив скрипт запуска:

```bash
sudo /usr/local/bin/zram-start.sh
```

Теперь вы можете убедится, что количество подкачки увеличилось на 2 Гб:

```bash
free -h
```

Всё работает, теперь можно проверить работает ли скрипт отключения:

```bash
sudo /usr/local/bin/zram-stop.sh
```

ШАГ 5. НАСТРОЙКА СЕРВИСА SYSTEMD

Чтобы запускать zram по умолчанию нужно создать для него юнит файл systemd. Создайте файл со следующим содержимым:

```bash
sudo systemctl edit --full --force zram.service
```

```bash
[Unit]

Description=zRAM block devices swapping

[Service]

Type=oneshot

ExecStart=/usr/local/bin/zram-start.sh

ExecStop=/usr/local/bin/zram-stop.sh

RemainAfterExit=yes

[Install]

WantedBy=multi-user.target
```

После этого выполните команду **daemon-reload**, чтобы перечитать конфигурацию systemd:

```bash
sudo systemctl daemon-reload
```

Запустите сервис systemd и добавьте его в автозагрузку:

```bash
sudo systemctl start zram

sudo systemctl enable zram
```

Готово! 
